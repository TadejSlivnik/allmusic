# -*- encoding: utf-8 -*-
from sys import version
if version.startswith('2'):
    from Tkinter import *
    import urllib2
else:
    from tkinter import *
    import urllib
import requests
import re
from time import sleep
import os


class Music():
    def __init__(self, master):
        self.frame = Frame(master)
        self.frame.grid(row=0, column=0, columnspan=2)
        self.frame1 = Frame(master)
        self.frame1.grid(row=1, column=0)
        self.frame2 = Frame(master)
        self.frame2.grid(row=1, column=1)
        self.frame3 = Frame(master)
        self.frame3.grid(row=2, column=1)
        self.frame4 = Frame(master)
        self.frame4.grid(row=2, column=0)

        self.albumlink = []
        self.song_link = []
        self.samples = []
        self.selected_song = ()
        self.type = ""

        Label(self.frame, text="Artist:").grid(row=0, column=0, padx=5)
        self.searching = StringVar()
        Label(self.frame, textvariable=self.searching).grid(row=1, column=1, padx=5)
        self.st = StringVar()
        search = Entry(self.frame, textvariable=self.st)
        search.grid(row=0, column=1, ipadx=15)
        search.focus_set()
        gumb_search = Button(self.frame, text="Search", command=self.search)
        search.bind("<Return>", self.search)
        gumb_search.grid(row=0, column=2, padx=1)

        Label(self.frame1, text="Albums: ").grid(row=0, column=0, padx=5)
        scrollbar1 = Scrollbar(self.frame1, orient=VERTICAL)
        self.listbox1 = Listbox(self.frame1, width=50, yscrollcommand=scrollbar1.set)
        self.listbox1.grid(row=0, column=1, ipadx=30, pady=5)
        self.current1 = ()
        self.listbox1.insert(END, "<no artist given>")
        scrollbar1.config(command=self.listbox1.yview)
        scrollbar1.grid(row=0, column=2, ipady=55)

        Label(self.frame2, text="Songs: ").grid(row=0, column=0, padx=5)
        scrollbar2 = Scrollbar(self.frame2, orient=VERTICAL)
        self.listbox2 = Listbox(self.frame2, width=50, yscrollcommand=scrollbar2.set)
        self.listbox2.grid(row=0, column=1, ipadx=60, pady=5)
        self.current2 = ()
        self.listbox2.insert(END, "<no artist given>")
        scrollbar2.config(command=self.listbox2.yview)
        scrollbar2.grid(row=0, column=2, ipady=55)
        gumb_sample = Button(self.frame2, text="Sample", command=self.song_sample)
        gumb_sample.grid(row=0, column=3, padx=10)

        scrollbar3 = Scrollbar(self.frame3)
        self.text = Text(self.frame3, wrap=WORD, yscrollcommand=scrollbar3.set)
        self.text.insert(END, "<no artist given>")
        self.text.config(state=DISABLED)
        self.text.grid(row=0, column=0, pady=5)
        scrollbar3.grid(row=0, column=1, ipady=170)
        scrollbar3.config(command=self.text.yview)

        scrollbar4 = Scrollbar(self.frame4)
        self.bio = Text(self.frame4, wrap=WORD, yscrollcommand=scrollbar4.set)
        self.bio.insert(END, "<no artist given>")
        self.bio.config(state=DISABLED)
        self.bio.grid(row=0, column=0, pady=5)
        scrollbar4.grid(row=0, column=1, ipady=170)
        scrollbar4.config(command=self.bio.yview)

    def poll1(self):
        now = self.listbox1.curselection()
        if now != self.current1:
            self.listbox_has_changed1(now)
            self.current1 = now
        self.frame.after(500, self.poll1)

    def listbox_has_changed1(self, selection):
        if selection != ():
            self.album(int(selection[0]))

    def poll2(self):
        now = self.listbox2.curselection()
        if now != self.current2 and self.type == "songs":
            self.listbox_has_changed2(now)
            self.current2 = now
        self.frame.after(500, self.poll2)

    def listbox_has_changed2(self, selection):
        if selection != ():
            self.song(int(selection[0]))

    def delete_lyrics(self):
        self.text.config(state=NORMAL)
        self.text.delete("1.0", END)
        self.text.insert(END, "<select a song to display lyrics>")
        self.text.config(state=DISABLED)

    def search(self, *args):
        self.searching.set("Searching for artists...")
        root.update_idletasks()
        sleep(1)
        self.artist()

    def artist(self):
        string_artist = self.st.get()
        self.selected_song = ()
        self.delete_lyrics()
        self.listbox2.delete(0, END)
        self.listbox1.delete(0, END)
        self.bio.config(state=NORMAL)
        self.bio.delete("1.0", END)
        self.text.config(state=NORMAL)
        self.text.delete("1.0", END)
        if string_artist != "":
            internet = True
            session = requests.Session()
            try:
                artist_links = session.get("http://www.allmusic.com/search/artists/" + string_artist)
            except requests.ConnectionError:
                self.listbox1.insert(END, "<no internet connection>")
                self.listbox2.insert(END, "<no internet connection>")
                self.bio.insert(END, "<no internet connection>")
                self.bio.config(state=DISABLED)
                self.text.insert(END, "<no internet connection>")
                self.text.config(state=DISABLED)
                internet = False
            if internet:
                self.listbox2.insert(END, "<please select an album>")
                self.bio.insert(END, "<no biography found>")
                self.bio.config(state=DISABLED)
                self.text.insert(END, "<please select a song>")
                self.text.config(state=DISABLED)
                first_link = re.findall('<a href="(http://www.allmusic.com/artist/.+)" ', artist_links.text)
                if first_link:
                    first_link = first_link[0]
                    stran = session.get(first_link)
                    ime = re.findall('<meta name="title" content="(.*)', stran.text)
                    ime = ime[0][:ime[0].find("|") - 1]
                    if "&auml;" in ime:
                        ime = ime.replace("&auml;", "ä")
                    elif "&Auml;" in ime:
                        ime = ime.replace("&Auml;", "Ä")
                    elif "&ouml;" in ime:
                        ime = ime.replace("&ouml;", "ö")
                    elif "&Ouml;" in ime:
                        ime = ime.replace("&Ouml;", "Ö")
                    elif "&uuml;" in ime:
                        ime = ime.replace("&uuml;", "ü")
                    elif "&Uuml;" in ime:
                        ime = ime.replace("&Uuml;", "Ü")
                    elif "&szlig;" in ime:
                        ime = ime.replace("&szlig;", "ß")
                    self.st.set(ime)
                    stran = first_link + "/biography"
                    stran = session.get(stran)
                    self.biografija(stran)
                    stran = first_link + "/discography"
                    stran = session.get(stran)
                    discography = re.findall('td class="year" data-sort-value="([\d\?]*-[\d\?]*-[\d\?]*)-(.*)' + '"',
                                             stran.text)
                    if discography:
                        self.type = "songs"
                        self.diskografija(stran, discography)
                    else:
                        self.text.config(state=NORMAL)
                        self.text.delete("1.0", END)
                        self.text.insert(END, "<no lyrics or samples for this artist>")
                        self.text.config(state=DISABLED)
                        self.listbox1.insert(END, "<artist has no albums>")
                        stran = first_link + "/compositions"
                        stran = session.get(stran)
                        komp = re.findall('<td class="year" data-sort-value="[\d\?]*-(.*)? for .*' + '">', stran.text)
                        if komp:
                            self.type = "compositions"
                            self.kompozicije(komp)
                        else:
                            self.type = ""
                            self.listbox2.delete(0, END)
                            self.listbox2.insert(END, "<no songs/compositions in database>")
                            self.poll2()
                else:
                    self.listbox1.insert(END, "<artist could not be found>")
                self.poll1()
        else:
            self.listbox1.insert(END, "<no text inserted>")
        self.searching.set("")

    def kompozicije(self, komp):
        kompoz = []
        kompoz1 = []
        for i in komp:
            if "&quot;" in i:
                kompoz1.append(i.replace("&quot;", "'"))
            else:
                kompoz1.append(i)
        for i in kompoz1:
            if "," in i:
                kompoz.append(i[:i.find(",")])
            else:
                kompoz.append(i)
        kompoz1 = []
        for i in kompoz:
            if i not in kompoz1:
                kompoz1.append(i)
        print(kompoz1)
        self.listbox2.delete(0, END)
        for i in kompoz1:
            self.listbox2.insert(END, i)
        self.poll2()

    def biografija(self, stran):
        bio = []
        bio0 = re.findall(
            '<h2 class="headline">(.*)</p>',
            stran.text, re.DOTALL)
        if bio0:
            bio0 = re.split("</h2>", bio0[0])
            biography_artist = bio0.pop(0)
            biography_artist = re.split("<.*?>", biography_artist)
            b_a = []
            for i in biography_artist:
                j = i.strip("\n")
                j = j.strip(" ")
                if j != "":
                    b_a.append(j)
            self.bio.config(state=NORMAL)
            self.bio.delete("1.0", END)
            bio_art = []
            for i in b_a:
                if "\n" in i:
                    j = i.split()
                    for a in j:
                        bio_art.append(a.strip(" "))
                else:
                    bio_art.append(i)
            line = ""
            for h in bio_art:
                line += " " + h
                line = line.strip(" ")
                line = line.replace(" ,", ",")
                line = line.replace(" .", ".")
            line += "\n\n"
            self.bio.insert(END, line)
            bio0 = "".join(bio0)
            bio0 = re.split("</p>", bio0)
            for i in bio0:
                j = i.strip("\n")
                bio.append(j.strip(" "))
            for i in bio:
                fah = []
                j = re.split("<.*?>", i)
                for h in j:
                    w = h.strip("\n")
                    w = w.strip(" ")
                    w = w.strip("\n")
                    w = w.strip(" ")
                    if w != "":
                        fah.append(w)
                line = ""
                for h in fah:
                    line += " " + h
                line = line.strip(" ")
                line = line.replace(" ,", ",")
                line = line.replace(" .", ".")
                line += "\n\n"
                self.bio.insert(END, line)
            self.bio.config(state=DISABLED)
        else:
            self.bio.config(state=NORMAL)
            self.bio.delete("1.0", END)
            self.bio.insert(END, "<no biography found>")
            self.bio.config(state=DISABLED)

    def diskografija(self, stran, discography):
        date = []
        date1 = []
        album1 = []
        for i in discography:
            date1.append(i[0])
            album1.append(i[1])
        if date1:
            for i in date1:
                h = i.split("-")
                date.append("{0}.{1}.{2} -".format(h[2], h[1], h[0]))
        if album1:
            for j, i in enumerate(album1):
                self.listbox1.insert(END, date[j] + " " + i)
            self.albumlink = re.findall('a href="(http://www.allmusic.com/album/.*)" data', stran.text)
        else:
            self.listbox1.insert(END, "<http://www.allmusic.com sadly has no album data for")
            self.listbox1.insert(END, "this artist...>")

    def album(self, number):
        self.selected_song = ()
        self.delete_lyrics()
        session = requests.Session()
        stran = session.get(self.albumlink[number])
        song_name = re.findall('<a href=".*" itemprop="url">(.*)</a>', stran.text)
        self.samples = re.findall(' <a href="#no-js" data-sample-url="(.*)" class="audio-player">', stran.text)
        link = re.findall('<a href="(.*)" itemprop="url">.*</a>', stran.text)
        self.song_link = []
        for i in link:
            self.song_link.append(i + "/lyrics")
        self.listbox2.delete(0, END)
        for i in song_name:
            self.listbox2.insert(END, i)
        self.poll2()

    def song(self, number):
        self.selected_song = number
        session = requests.Session()
        stran = session.get(self.song_link[number])
        f = re.findall(
            '<p id="hidden_without_js" style="display: none;">(.*)<img src="http://assets.allmusic.com/images/transparent.png">',
            stran.text, re.DOTALL)
        f = f[0]
        f = re.split("<br />", f)
        seznam_besedila = []
        for i in f:
            j = i.strip("\r")
            k = j.strip("\n")
            seznam_besedila.append(k.strip(" "))
        if seznam_besedila == [""]:
            self.text.config(state=NORMAL)
            self.text.delete("1.0", END)
            self.text.insert(END, "<no lyrics found, please try another song...>")
            self.text.config(state=DISABLED)
        else:
            besedilo = ""
            for i in seznam_besedila:
                besedilo = besedilo + i + "\n"
            self.text.config(state=NORMAL)
            self.text.delete("1.0", END)
            self.text.insert(END, besedilo)
            self.text.config(state=DISABLED)

    def song_sample(self):
        if self.selected_song != ():
            if version.startswith('2'):
                request = urllib2.Request(self.samples[self.selected_song])
                page = urllib2.urlopen(request)
            else:
                request = urllib.request.Request(self.samples[self.selected_song])
                page = urllib.request.urlopen(request)
            fname = "temp_allmusic_sample.wav"
            f = open(fname, 'wb')
            f.write(page.read())
            f.close()
            os.system(fname)
            # os.system("vlc "+fname) # odpre nameščen vlc  # linux?


root = Tk()
root.title("AllMusic info")
# root.state('zoomed')  # windows
# root.attributes('-zoomed', True)  # linux?
aplikacija = Music(root)
root.mainloop()